# SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

# This file is only required by pip-tools and older versions of pip now
from setuptools import setup

setup()
