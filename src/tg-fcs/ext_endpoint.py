from typing import Any, Dict, List, Optional, Union
import logging

from flask import Flask, Response, request

import cql
from clarin.sru.server.request import SRURequest
from clarin.sru.server.result import SRUSearchResultSet
from clarin.sru.fcs.constants import FCS_NS, FCSQueryType
from clarin.sru.queryparser import CQLQuery, SRUQuery, SRUQueryParserRegistry
from clarin.sru.diagnostic import SRUDiagnostic, SRUDiagnosticList
from clarin.sru.fcs.xml.writer import FCSRecordXMLStreamWriter
from clarin.sru.fcs.queryparser import FCSQuery
from query_converter import fcs2cqp

from clarin.sru.exception import SRUException

from tgclients import TextgridSearch
from tgclients.databinding.tgsearch import Response as SearchResponse, ResultType, FulltextType


from clarin.sru.constants import (
    SRUDiagnostics,
    SRUResultCountPrecision,
    SRUVersion,
)

from clarin.sru.server.config import (
    DatabaseInfo,
    IndexInfo,
    LocalizedString,
    SchemaInfo,
    SRUServerConfig,
    SRUServerConfigKey,
)

from clarin.sru.fcs.server.search import (
    DataView,
    Layer,
    EndpointDescription,
    ResourceInfo,
    SimpleEndpointDescription,
    SimpleEndpointSearchEngineBase,
)

from clarin.sru.server.server import SRUServer
from clarin.sru.xml.writer import SRUXMLStreamWriter, XMLStreamWriterHelper

MIMETYPE_TEI_XML = "application/x-tei+xml"

logger = logging.getLogger(__name__)
app = Flask(__name__)

def _cql2cqp(query: CQLQuery) -> str:
    node: Union[
        cql.parser.CQLTriple, cql.parser.CQLSearchClause
    ] = query.parsed_query.root

    if isinstance(node, cql.parser.CQLTriple):
        operator = node.operator.value
        raise SRUException(
            SRUDiagnostics.UNSUPPORTED_BOOLEAN_OPERATOR,
            operator,
            message=f"Unsupported Boolean operator: {operator}",
        )

    if isinstance(node, cql.parser.CQLSearchClause):
        terms = node.term.lower().split()  # .casefold()?
        if len(terms) == 1:
            return f"{terms[0]}"

        terms = [term.strip("\"'") for term in terms]
        return " ".join(f"{term}" for term in terms)

    raise SRUException(
        SRUDiagnostics.CANNOT_PROCESS_QUERY_REASON_UNKNOWN,
        f"unknown cql node: {node}",
    )

class TgFCSSearchResultSet(SRUSearchResultSet):

    def __init__(
        self,
        results: SearchResponse,
        diagnostics: SRUDiagnosticList,
        resource_pid: str,
        request: Optional[SRURequest] = None,
    ) -> None:
        super().__init__(diagnostics)
        self.request = request
        self.results = results
        self.resource_pid = resource_pid

        if request:
            self.start_record = max(1, request.get_start_record())
            self.current_record_cursor = self.start_record - 1
            self.maximum_records = (
                self.start_record - 1 + request.get_maximum_records()
            )
            self.record_count = request.get_maximum_records()
        else:
            self.start_record = 1
            self.current_record_cursor = self.start_record - 1
            self.maximum_records = 250
            self.record_count = 250

    def get_record_count(self) -> int:
        #if self.results and len(self.results) > -1:
        #    if len(self.results) < self.maximum_records:
        #        return len(self.results)
        #    else:
        #        return self.maximum_records
        #return 0
        return len(self.results.result)

    def get_total_record_count(self) -> int:
        if self.results:
            return len(self.results.hits)
        return -1

    def next_record(self) -> bool:
        if self.current_record_cursor < min(
            len(self.results.result), self.maximum_records
        ):
            self.current_record_cursor += 1
            #print(self.current_record_cursor)
            return True
        return False

    def get_record_identifier(self) -> str:
        return None

    def get_record_schema_identifier(self) -> str:
        if self.request:
            rsid = self.request.get_record_schema_identifier()
            if rsid:
                return rsid
        return FCS_NS  # CLARIN_FCS_RECORD_SCHEMA

    def write_record(self, writer: SRUXMLStreamWriter) -> None:
        result: ResultType = self.results.result[self.current_record_cursor-1]

        #print(result.object_value.generic.provided.title[0])

        tguri:str = result.object_value.generic.generated.textgrid_uri.value


        FCSRecordXMLStreamWriter.startResource(writer, pid=tguri)
        FCSRecordXMLStreamWriter.startResourceFragment(writer, pid=f"{tguri}_1")

        if len(result.fulltext) > 0:
            kwic: FulltextType.Kwic = result.fulltext[0].kwic[0]
            #print(kwic.match)

            FCSRecordXMLStreamWriter.writeSingleHitHitsDataView(
                writer, kwic.left, kwic.match, kwic.right
            )

        FCSRecordXMLStreamWriter.endResourceFragment(writer)
        FCSRecordXMLStreamWriter.endResource(writer)

class TgFCSEndpointSearchEngine(SimpleEndpointSearchEngineBase):

    def __init__(
        self,
        endpoint_description: EndpointDescription,
    #    lexicon: Dict[str, Any],
    #    field: str = "xml",
    #    query_type: str = "term",
    ) -> None:
        super().__init__()
        self.endpoint_description = endpoint_description
    #    self.lexicon = lexicon
    #    self.field = field
    #    self.query_type = query_type

    def create_EndpointDescription(
        self,
        config: SRUServerConfig,
        query_parser_registry_builder: SRUQueryParserRegistry.Builder,
        params: Dict[str, str],
    ) -> EndpointDescription:
        return self.endpoint_description

    def do_init(
        self,
        config: SRUServerConfig,
        query_parser_registry_builder: SRUQueryParserRegistry.Builder,
        params: Dict[str, str],
    ) -> None:
        pass

    def search(
        self,
        config: SRUServerConfig,
        request: SRURequest,
        diagnostics: SRUDiagnosticList,
    ) -> SRUSearchResultSet:
        query: str

        if request.is_query_type(FCSQueryType.CQL):
            # Got a CQL query (either SRU 1.1 or higher).
            # Translate to a plain query string ...
            query_in: SRUQuery = request.get_query()
            assert isinstance(query_in, CQLQuery)
            query = _cql2cqp(query_in)
            #query = query_in
        elif request.is_query_type(FCSQueryType.FCS):
            # Got a FCS query (SRU 2.0).
            # Translate to a proper CQP query
            query_in: SRUQuery = request.get_query()
            assert isinstance(query_in, FCSQuery)
            query = fcs2cqp(query_in)
        else:
            # Got something else we don't support. Send error ...
            raise SRUException(
                SRUDiagnostics.CANNOT_PROCESS_QUERY_REASON_UNKNOWN,
                f"Queries with queryType '{request.get_query_type()}' are not supported by this CLARIN-FCS Endpoint.",
            )
        
        #logger.info(query)
        print("QUERY!")
        print(query)

        # NOTE: theoretically check for correct resource pid but since each endpoint uses its own route, it should not matter
        #self._parse_params(request, diagnostics)

        #search = TextgridSearch()
        #results = search.search(query, limit=request.get_maximum_records())

        # now again, try autocompletion for navigating the databinding
        #title = results.result[0].object_value.generic.provided.title[0]
        #return TgFCSSearchResultSet(
            #results, diagnostics=diagnostics, resource_pid=self.lexicon.uid
        #    results, diagnostics=diagnostics, resource_pid="TODO-PID"
        #)


class TgFcs:

    def __init__(self):
        self.server = self.build_fcs_server()

    def build_fcs_server_params(self) -> Dict[str, Any]:
        return {
            SRUServerConfigKey.SRU_SUPPORTED_VERSION_MIN: SRUVersion.VERSION_1_1.version_string,
            SRUServerConfigKey.SRU_SUPPORTED_VERSION_MAX: SRUVersion.VERSION_2_0.version_string,
            SRUServerConfigKey.SRU_SUPPORTED_VERSION_DEFAULT: SRUVersion.VERSION_2_0.version_string,
            SRUServerConfigKey.SRU_TRANSPORT: "http",
            #SRUServerConfigKey.SRU_HOST: instance.config["api"]["host"],
            #SRUServerConfigKey.SRU_PORT: instance.config["api"]["port"],
            SRUServerConfigKey.SRU_HOST: "localhost",
            SRUServerConfigKey.SRU_PORT: "5000",            
            #SRUServerConfigKey.SRU_DATABASE: self.path,
            SRUServerConfigKey.SRU_DATABASE: "/fcs",
            SRUServerConfigKey.SRU_ECHO_REQUESTS: "true",
            SRUServerConfigKey.SRU_ALLOW_OVERRIDE_MAXIMUM_RECORDS: "true",
            SRUServerConfigKey.SRU_ALLOW_OVERRIDE_MAXIMUM_TERMS: "false",
            SRUServerConfigKey.SRU_ALLOW_OVERRIDE_INDENT_RESPONSE: "true",
        }

    def build_fcs_server_config(
        self, params: Dict[str, Any]
    ) -> SRUServerConfig:
        database_info = (
            DatabaseInfo(
                title=[
                    LocalizedString(
                        value="TG TEST", lang="en", primary=True
                    )
                ],
                author=[
                    LocalizedString(
                        value=author,
                        lang="en",
                        primary=True if i == 0 else False,
                    )
                    for i, author in enumerate(["TODO"])
                ],
            )
        )
        index_info = (
            IndexInfo(
                sets=[
                    IndexInfo.Set(
                        identifier="http://clarin.eu/fcs/resource",
                        name="fcs",
                        title=[
                            LocalizedString(
                                value="CLARIN Content Search",
                                lang="en",
                                primary=True,
                            )
                        ],
                    )
                ],
                indexes=[
                    IndexInfo.Index(
                        can_search=True,
                        can_scan=False,
                        can_sort=False,
                        maps=[
                            IndexInfo.Index.Map(
                                primary=True, set="fcs", name="words"
                            )
                        ],
                        title=[
                            LocalizedString(
                                value="Words", lang="en", primary=True
                            )
                        ],
                    )
                ],
            )
        )
        schema_info = [
            SchemaInfo(
                identifier="http://clarin.eu/fcs/resource",
                name="fcs",
                location=None,
                sort=False,
                retrieve=True,
                title=[
                    LocalizedString(
                        value="CLARIN Content Search",
                        lang="en",
                        primary=True,
                    )
                ],
            )
        ]

        return SRUServerConfig.fromparams(
            params,
            database_info=database_info,
            index_info=index_info,
            schema_info=schema_info,
        )

    def build_fcs_endpointdescription(self) -> EndpointDescription:
        dataviews = [
            DataView(
                identifier="hits",
                mimetype="application/x-clarin-fcs-hits+xml",
                deliveryPolicy=DataView.DeliveryPolicy.SEND_BY_DEFAULT,
            ),
            DataView(
                identifier="tei",
                mimetype=MIMETYPE_TEI_XML,
                deliveryPolicy=DataView.DeliveryPolicy.SEND_BY_DEFAULT,
            ),
        ]
        resources = [
            ResourceInfo(
                pid="TODO_PID", # TODO eindeutiger identifier der resource (projekt-id z.b.)
                title={"en": "TITLE"}, # TODO title der resource (projekt name)
                description=None,
                landing_page_uri=None,
                languages=["DE", "EN"], # TODO
                available_DataViews=dataviews,
            )
        ]
        layers = [
            Layer(
                id="word",
                result_id="http://localhost/wordlayer",
                type="TODO",
                encoding="utf-8"
            ),
            Layer(
                id="pos",
                result_id="http://localhost/poslayer",
                type="TODO",
                encoding="utf-8"
            )
        ]
        return SimpleEndpointDescription(
            version=2,
            capabilities=["http://clarin.eu/fcs/capability/basic-search", "http://clarin.eu/fcs/capability/advanced-search"],
            supported_DataViews=dataviews,
            supported_Layers=layers,
            resources=resources,
            pid_case_sensitive=False,
        )

    def build_fcs_server(self) -> SRUServer:
        params = self.build_fcs_server_params()
        config = self.build_fcs_server_config(params)
        qpr_builder = SRUQueryParserRegistry.Builder(True)
        search_engine = TgFCSEndpointSearchEngine(
            endpoint_description=self.build_fcs_endpointdescription(),
            #lexicon=self.lexicon,
            #field=self.field,
            #query_type=self.query_type,
        )
        search_engine.init(config, qpr_builder, params)
        return SRUServer(config, qpr_builder.build(), search_engine)



tgfcs = TgFcs()

@app.route("/efcs")
def handle() -> Response:
    logger.debug("request: %s", request)
    logger.debug("request?args: %s", request.args)
    response = Response()
    tgfcs.server.handle_request(request, response)
    return response
