# fcs endpoint

**Note:** This is the experimental (protoype) version of the FCS service, also containing some code samples for further developments like extended search features. The stable codebase for running textgrid FCS service (only fulltext search) is developed at https://gitlab.gwdg.de/textplus/collections/tg-fcs-endpoint

## run

setup

```bash
python -m venv venv
. venv/bin/activate
pip install -e .

flask --app src/tg-fcs/endpoint run
```

and open

<http://localhost:5000/fcs?query=alice&maximumRecords=10>


## examples:

simple search (in all projects):

* <http://localhost:5000/fcs?query=alice&maximumRecords=10>

simple search within project "Digitale Bibliothek"

* http://localhost:5000/TGPR-372fe6dc-57f2-6cd4-01b5-2c4bbefcfd3c/fcs?query=alice&maximumRecords=10

extended search:

* [ pos = "ADV" ]{2,4} - http://localhost:5000/fcs?queryType=fcs&query=[%20pos%20=%20%22ADV%22%20]{2,4}
* [ word = "Hai" ] - http://localhost:5000/fcs?queryType=fcs&query=[%20word%20=%20%22Hai%22%20]
* [ pos = "DET" ] [ pos = "ADJ" ] [ word = "Hai" ] - http://localhost:5000/fcs?queryType=fcs&query=[%20pos%20=%20%22DET%22%20]%20[%20pos%20=%20%22ADJ%22%20]%20[%20word%20=%20%22Hai%22%20]


## references

* https://github.com/Querela/fcs-korp-endpoint-python/
* https://github.com/cceh/kosh/commit/1785582775dde1839e4b2f106625e0bd79d86141
* https://docs.google.com/presentation/d/16X5b0eUYjSupwuhdJZevgLNvzWQ6U6-YdgGOtwKwqDI/edit#slide=id.g296a7d5e169_0_29


## start fcs aggregator locally

```bash
$ git clone https://github.com/clarin-eric/fcs-sru-aggregator.git && cd fcs-sru-aggregator
$ ./build.sh --jar
```

edit conf

```diff
diff --git a/aggregator_devel.yml b/aggregator_devel.yml
index 25424dd..2387724 100644
--- a/aggregator_devel.yml
+++ b/aggregator_devel.yml
@@ -1,5 +1,5 @@
 aggregatorParams:
-  CENTER_REGISTRY_URL: https://centres.clarin.eu/restxml/
+  #CENTER_REGISTRY_URL: https://centres.clarin.eu/restxml/
   #additionalCQLEndpoints:
     # - https://clarin.ids-mannheim.de/digibibsru-new
     #- http://localhost:8080/korp-endpoint/sru
@@ -13,17 +13,19 @@ aggregatorParams:
     #- url: https://spraakbanken.gu.se/ws/fcs/2.0/endpoint/korp/sru  # same as simple string
     #- name: abc  # --> error, URL required
 
-  #additionalFCSEndpoints:
+  additionalFCSEndpoints:
     #- http://localhost:8080/korp-endpoint/sru
     #- https://spraakbanken.gu.se/ws/fcs/2.0/endpoint/korp/sru
+    - url: http://localhost:5000/fcs
+      name: localtest
 
   AGGREGATOR_FILE_PATH: fcsAggregatorResources.json
   AGGREGATOR_FILE_PATH_BACKUP: fcsAggregatorResources.backup.json
 
   SCAN_MAX_DEPTH: 1 # recommended 3
   SCAN_TASK_INITIAL_DELAY: 0
-  SCAN_TASK_INTERVAL: 12
-  SCAN_TASK_TIME_UNIT: HOURS
+  SCAN_TASK_INTERVAL: 5
+  SCAN_TASK_TIME_UNIT: MINUTES
   SCAN_MAX_CONCURRENT_REQUESTS_PER_ENDPOINT: 4  # @depth2: 1=361s; 2=225s; 4=207s
 
   SEARCH_MAX_CONCURRENT_REQUESTS_PER_ENDPOINT: 4
@@ -73,7 +75,7 @@ logging:
   # Logger-specific levels.
   loggers:
     eu.clarin.sru.fcs.aggregator: INFO
-    eu.clarin.sru.client: WARN
+    eu.clarin.sru.client: DEBUG
 
   appenders:
     - type: console
```

start

```bash
./build.sh --run
```

# ext endpoint

```bash
flask --app src/tg-fcs/ext_endpoint run
```

simple search: 

* <http://localhost:5000/efcs?query=alice&maximumRecords=10>

extended search:

* [ pos = "ADV" ]{2,4} - http://localhost:5000/efcs?queryType=fcs&query=[%20pos%20=%20%22ADV%22%20]{2,4}
* [ word = "Hai" ] - http://localhost:5000/efcs?queryType=fcs&query=[%20word%20=%20%22Hai%22%20]
* [ pos = "DET" ] [ pos = "ADJ" ] [ word = "Hai" ] - http://localhost:5000/efcs?queryType=fcs&query=[%20pos%20=%20%22DET%22%20]%20[%20pos%20=%20%22ADJ%22%20]%20[%20word%20=%20%22Hai%22%20]

* [ pos = "NOUN" & word = "Buch" ] - http://localhost:5000/efcs?queryType=fcs&query=[%20pos%20=%20%22NOUN%22%20%26%20word%20=%20%22Buch%22%20]

