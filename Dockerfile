# TODO: builder (not slim) and app (slim)
FROM python:3.11
RUN mkdir /app
WORKDIR /app/
ADD . /app/
RUN pip install .
RUN pip install gunicorn
CMD ["gunicorn", "--bind", "0.0.0.0:80", "tg-fcs.endpoint:app"]
